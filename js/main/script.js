var mainApp = angular.module('mainApp', []);

// configure interpolation
	mainApp.config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[').endSymbol(']]');
	});
  mainApp.directive("ngFileSelect",function(){
    // console.log("fileGet");
      return {
        link: function($scope,el){

          el.bind("change", function(e){
          // console.log("onFileSelect");
            $scope.file = (e.srcElement || e.target).files[0];
          })

        }

      }
    });


// Define the `PhoneListController` controller on the `phonecatApp` module
mainApp.controller('mainController', function mainController($scope,$sce) {
function makeid(lenght)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < lenght; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
$scope.uploadFile = function()
{
  var storageRef = firebase.storage().ref();
  if($scope.file != undefined)
  {
    var file = $scope.file;
    var random_name = makeid(20);
    var file_name = file.name;
    var file_type = file.type;
    var file_name_split = file_name.split(".");
    var file_name_split_length = file_name_split.length;
    var file_type_name = file_name_split[file_name_split_length-1];
    var file_type_name_lower = file_type_name.toLowerCase();
    var new_file_name = random_name+"."+file_type_name;
    if (file.type.match('image.*')) {
      //Uplad Pictures
      storageRef.child('Pictures/'+new_file_name).put(file).then(function(snapshot) {
        //Get Url
        storageRef.child('Pictures/'+new_file_name).getDownloadURL().then(function(url) {
          // console.log(url.split("%"));
          var data = "url1="+url.split("%")[0]+"&url2="+url.split("%")[1];
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
          if(this.readyState == 4 && this.status == 200) {
            // console.log(this.responseText);
              window.location.replace("home");
            }
          };
      
          xhttp.open("POST", baseUrl+"main/editImgProfile");
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send(data);
          });
        });

      }

  }
  
}
});

// var checkedPass = function()
// {
//   $(document).ready(function() {
//       var password = document.getElementById("password").value;
//       var repassword = document.getElementById("repassword").value;
//       if(password == repassword)
//       {
//         if(password.length >5)
//         {
//          $('#submitReset').prop('disabled', false);
//           document.getElementById("checkedPassText").innerHTML = "";
//         }else
//         {
//           $('#submitReset').prop('disabled', true);
//           document.getElementById("checkedPassText").innerHTML = "ต้องมีตัวอักษรมากกว่า 5 ตัวขึ้นไป";
//         }
//       }
//       else
//       {
//         $('#submitReset').prop('disabled', true);
//         document.getElementById("checkedPassText").innerHTML = "รหัสไม่ตรงกัน";
//       }
     
//   });
// }
//   $(document).ready(function() {
//      $('#submitReset').prop('disabled', true);
//   });