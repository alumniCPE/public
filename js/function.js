var producted ;
var first_order = 0;
var old_value = 0;
var index = 0;
var stack = [];


Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

function add_number(col1,col2,col3,col4,table,main_orders,products_count)
{
    
   	var first_number = document.getElementById(col1).value;

    var pos = first_number.indexOf(",");
    while (pos > 0) {
        first_number = first_number.replace(",","");
        pos = first_number.indexOf(",");
    } 
    var second_number = parseInt(document.getElementById(col2).value);
    if(second_number < 0)
    {
      second_number = 0;
      document.getElementById(col2).value = 0;
    }
    if(second_number > main_orders)
    {
      second_number = main_orders;
      document.getElementById(col2).value = 0;
    }
    if(!isNaN(parseInt(document.getElementById(col4).value)))
    {
      document.getElementById(col4).value = main_orders - second_number;
      var result = first_number * second_number;
      document.getElementById(col3).value = (result).formatMoney(2, '.', ',');
    }else
    {
      var result = first_number * second_number;
      document.getElementById(col3).value = (result).formatMoney(2, '.', ',');
      myCreateFunction(table,col4,second_number)
    }

     total_count_amount(products_count);
            
}

function amount_cal(col1,col2,col3,products_count)
{
    //console.log(products_count);
    var first_number = document.getElementById(col1).value;
    var pos = first_number.indexOf(",");
    while (pos > 0) {
        first_number = first_number.replace(",","");
        pos = first_number.indexOf(",");
    } 
    var second_number = parseInt(document.getElementById(col2).value);
    var result = first_number * second_number;
    document.getElementById(col3).value = (result).formatMoney(2, '.', ',');
    total_count_amount(products_count);
            
}

function myCreateFunction(table,col,cells) {

var table = document.getElementById(table);
 //first
 if(col != producted)
 {
    first_order = 0;
    old_value = 0;
    index = 0;
 }
 try
 {
    for (var i = 0; i <= stack.length; i++) 
    {
       if(stack[i]["main"] == col)
      { 
          index = stack[i]["index"];
          first_order = stack[i]["orders"];
          old_value = stack[i]["old_value"];
          break;
      }
    }
 }catch(err) {
   
}
    var value = cells - old_value;
    // console.log('value'+value);
    // console.log('index'+index);
    // console.log('order'+first_order);
    if(value > 0)
    {
      if(first_order <= 0)
      {
       first_order=1;
      }
        for (var i = 1; i <= value; i++) {
        index++;
        row = table.insertRow(first_order);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerHTML = index;
        cell2.innerHTML = "<input type='number' min='1' max='6999' maxlength='4' class='form-control' id='"+col+"-"+index+"' name='"+col+"-"+index+"' onclick=check_number('"+col+"-"+index+"','note_"+col+"-"+index+"','"+col+"') onkeypress=check_number('"+col+"-"+index+"','note_"+col+"-"+index+"','"+col+"')>";
        cell3.innerHTML = "<input type='text' min='1' max='6999' class='form-control' id='note_"+col+"-"+index+"' name='note_"+col+"-"+index+"' readonly>";
        first_order++;
        }
  //console.log('+');
    }else if(value < 0)
    {
      
      if(first_order <= 0)
      {
       first_order=1;
      }
      for (var i = 1; i <= Math.abs(value); i++) {
        table.deleteRow(first_order-1);
        //console.log('ordered'+first_order);
        first_order--;
        index = first_order;

      }
      index = first_order-1;
       
    }

    old_value = cells;
    


 producted = col;
 
for (var i = 0; i <= stack.length; i++) 
{
  if (stack[i] === undefined)
  { 
    stack.push({"main":col,"index":index,"orders":first_order,"old_value":old_value});
    break;
  }else
  {
    //update
    if(stack[i]["main"] == col)
    {
      stack[i] = {"main":col,"index":index,"orders":first_order,"old_value":old_value};
      // console.log(stack[i]["main"]);
      // console.log(stack[i]["index"]);
      // console.log(stack[i]["orders"]);
      // console.log(stack[i]["old_value"]);
      break;
    }
    
  }
  //console.log("new");
  stack.push({"main":col,"index":index,"orders":first_order,"old_value":old_value});


}
}
function check_number(col_number,col_note,col_main)
{
  var number = parseInt(document.getElementById(col_number).value);
  if(number > 6999)
  {
    document.getElementById(col_number).value = 699;
    document.getElementById(col_note).value = "หมายเลขเกินจำนวนการจอง";
  }
  var product_id = col_main.substring(4);
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) 
    {
      if(xmlhttp.responseText == "true")
      {
         document.getElementById(col_note).value = "หมายเลขถูกจองแล้ว";
      }else
      {
        document.getElementById(col_note).value = "หมายเลขยังไม่ถูกจอง";
      }
    }
  }
    xmlhttp.open("GET","http://localhost/lks/order/check_number?number="+number+"&product_id="+product_id,true);
    xmlhttp.send();
    // document.getElementById(col_note).innerHTML=xmlhttp.responseText;
}


function popup_module(modal,close,col1,col2,col3,col4,table,main_orders,products_count)
{
add_number(col1,col2,col3,col4,table,main_orders,products_count);

var modal = document.getElementById(modal);

// Get the <span> element that closes the modal
var span = document.getElementsByClassName(close)[0];

// When the user clicks the button, open the modal
modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

}

function popup_module_pin(modal,close,col1,col2,col3,col4,table,main_orders,products_count,product_id,customer_id)
{
  add_number(col1,col2,col3,col4,table,main_orders,products_count);
  var modal = document.getElementById(modal);
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName(close)[0];
  // When the user clicks the button, open the modal
  modal.style.display = "block";
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
  modal.style.display = "none";
  }

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) 
    {
    }
  }
    xmlhttp.open("GET","http://localhost/lks/order/delete_pin?product_id="+product_id+"&customer_id="+customer_id,true);
    xmlhttp.send();

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
  }

}



function total_count_amount(count_rows)
{

  var total_count = 0;
  var total_amount = 0;
  for (var i=1; i <= count_rows; i++) 
  {
    var str_count = "count"+i;
    var str_result = "result"+i;
    try {
          total_count  = total_count  + parseInt(document.getElementById(str_count).value);
        }
    catch(err) {
        }
    try {
            var order_result = document.getElementById(str_result).value;
            var pos = order_result.indexOf(",");
            while (pos > 0) {
                order_result = order_result.replace(",","");
                pos = order_result.indexOf(",");
            } 
            total_amount = total_amount + parseFloat(order_result);
        }
    catch(err) {
        }

    
    
  }
  document.getElementById('total_count').value = total_count;
  document.getElementById('total_amount').value = (total_amount).formatMoney(2, '.', ',');

}

function order_popup()
{
  console.log("popup");
}




