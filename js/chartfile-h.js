$(function () {

var limit = 10;

var i = 0; var fieldData = [];
for(fields in workfield) {
  fieldData[i] = {};
  fieldData[i].name = fields;
  fieldData[i].y = workfield[fields];
  i++;
  if(i > limit) { break; }
}

var j = 0; var regionData = [];
for(regions in region) {
  regionData[j] = {};
  regionData[j].name = regions;
  regionData[j].y = region[regions];
  j++;
  if(j > limit) { break; }
}

var k = 0; var workData = [];
for(works in companywork) {
  workData[k] = {};
  workData[k].name = works;
  workData[k].y = companywork[works];
  k++;
  if(k > limit) { break; }
}

/*workfield*/
Highcharts.chart('workfield', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} คน</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    //chart data
    series: [{
        name: 'จำนวน',
        colorByPoint: true,
        data: fieldData
    }]
});

/*region*/
Highcharts.chart('region', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} คน</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    //chart data
    series: [{
        name: 'จำนวน',
        colorByPoint: true,
        data: regionData
    }]
});

/*companyWork*/
Highcharts.chart('company', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} บริษัท</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    //chart data
    series: [{
        name: 'จำนวน',
        colorByPoint: true,
        data: workData
    }]
});


});
