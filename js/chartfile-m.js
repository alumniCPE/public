jQuery(document).ready(function(){
  var limit = 10;
  new Morris.Bar({
    element: amount,
    data: stdamount,
    xkey: 'year',
    ykeys: ['cpe', 'isne'],
    labels: ['CPE', 'ISNE'],
    ymax: 80,
    ymin: 0,
    xLabelMargin: 10,
    hideHover: true,
    postUnits: " คน",
    gridIntegers: true,
    resize: true
  });


  var i = 0; var fieldData = [];
  for(fields in workfield) {
    fieldData[i] = {};
    fieldData[i].label = fields;
    fieldData[i].value = workfield[fields];
    i++;
    if(i > limit) { break; }
  }

  var j = 0; var regionData = [];
  for(regions in region) {
    regionData[j] = {};
    regionData[j].label = regions;
    regionData[j].value = region[regions];
    j++;
    if(j > limit) { break; }
  }

  var k = 0; var workData = [];
  for(works in companywork) {
    workData[k] = {};
    workData[k].label = works;
    workData[k].value = companywork[works];
    k++;
    if(k > limit) { break; }
  }

  new Morris.Donut({
    element: 'workfield',
    data: fieldData,
    formatter: function(y,data) { return y + ' คน'},
    colors: ["#62bfe4","#73c7e7","#87ceeb","#96d5ed","#a8dcf0","#b9e3f3","#cbeaf6","#dcf1f9"],
    resize: true
  });

  new Morris.Donut({
    element: 'trend',
    data: workData,
    formatter: function(y,data) { return y + ' บริษัท'},
    colors: ["#f50000","#ff0000","#ff1f1f","#ff3333","#ff4747","#ff5c5c","#ff7070","#ff8585"],
    resize: true
  });

  new Morris.Donut({
    element: 'region',
    data: regionData,
    formatter: function(y,data) { return y + ' คน'},
    colors: ["#38a85c","#3db864","#47c26e","#57c77a","#66cc86","#75d192","#85d69e","#94dbab"],
    resize: true
  });

});
