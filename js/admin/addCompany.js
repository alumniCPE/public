var addCompanyApp = angular.module('addCompany', []);

// configure interpolation
	addCompanyApp.config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[').endSymbol(']]');
	});

// Define the `PhoneListController` controller on the `phonecatApp` module
addCompanyApp.controller('addcompanyController', function addcompanyController($scope,$sce) {
  $scope.checkCompany = function()
  {	
  	 var result = [];
  	 var data = "name="+$scope.companyName;
	   var xhttp = new XMLHttpRequest();
	   xhttp.onreadystatechange = function() {
	   if (this.readyState == 4 && this.status == 200) {
      if(this.responseText != ""){
	   		result = JSON.parse(this.responseText);
	   		$scope.companyName = result.name;
	   		$scope.companyAddress = result.address;
	   		$scope.companyCity = result.city;
	   		$scope.companyPhone = result.phone;
	   		$scope.companyWebsite = result.website;
        $scope.companyFacebook = result.facebook;

        var companyWork = result.work;
        for (var i = 0; i < companyWork.length; i++) {
          $(document).ready(
            function(){
                var theValue = companyWork[i];
                $('#companyWork option[value=' + theValue + ']')
                    .attr('selected',true);
            });
        }

        $("#companyWork").selectpicker("refresh");
      }
        	   }
        	};

	xhttp.open("POST", baseUrl+"admin/checkCompany",false);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
  }
});

//Auto Complete
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

  var listCompany = [];
  var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
        listCompany = JSON.parse(this.responseText);
     }
  };
  xhttp.open("GET", baseUrl+"admin/listCompany",false);
  xhttp.send();


$('.listCompany').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'listCompany',
  source: substringMatcher(listCompany)
});


var checkedParameter = function()
{
  $(document).ready(function() {
      var companyName = document.getElementsByName("companyName")[0].value;
      var companyAddress = document.getElementsByName("companyAddress")[0].value;
      var companyCity = document.getElementsByName("companyCity")[0].value;
      var userWork = document.getElementById("userWork").value;
      var companyWork = document.getElementById("companyWork").value;
      var startWork = document.getElementsByName("startWork")[0].value;
      var endWork = document.getElementsByName("endWork")[0].value;
      var companyPhone = document.getElementsByName("companyPhone")[0].value;
      var companyWebsite = document.getElementsByName("companyWebsite")[0].value;
      // console.log(companyName);
      // console.log(companyAddress);
      // console.log(companyCity);
      // console.log(userWork);
      // console.log(companyWork);
      // console.log(startWork);
      // console.log(endWork);
      // console.log(companyPhone);
      // console.log(companyWebsite);
      if(companyName != undefined && companyAddress != undefined && companyCity != "" && userWork != "" && companyWork != "" && startWork != "" && endWork != "" && companyPhone != undefined && companyWebsite != undefined)
      {
      	$('#addCompanyButton').prop('disabled', false);
      	document.getElementById("checkedParameterText").innerHTML = "";
      }else
      {
      	$('#addCompanyButton').prop('disabled', true);
      	document.getElementById("checkedParameterText").innerHTML = "ข้อมูลยังไม่สมบูรณ์";
      }
  });
}
$('#addCompanyButton').prop('disabled', true);

