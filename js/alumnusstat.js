$(function() {

var amt_cpe = [];
var amt_isne = [];
var year = []
var amt = 0;

for(amt = 0; amt < stdamount.length; amt++) {
  year[amt] = amt+2535;
  amt_cpe[amt] = stdamount[amt].cpe;
  amt_isne[amt] = stdamount[amt].isne;
}


Highcharts.chart('amount', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: year,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'จำนวนนักศึกษา (คน)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:1.2em">พ.ศ.{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y} คน</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'CPE',
        data: amt_cpe

    }, {
        name: 'ISNE',
        data: amt_isne

    }]
});

});
