var registerApp = angular.module('registerApp', []);

// configure interpolation
	registerApp.config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[').endSymbol(']]');
	});

// Define the `PhoneListController` controller on the `phonecatApp` module
registerApp.controller('registerController', function registerController($scope,$sce) {
  $scope.callApiGet = function(url)
  {
    var data = [];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
       if (this.readyState == 4 && this.status == 200) {
          data = JSON.parse(this.responseText);
       }
    };
    xhttp.open("GET", baseUrl+""+url,false);
    xhttp.send();
    return data;
  }

//   var myHTML = "<h1>aaaa</h1>";
// $scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml(myHTML);
  $scope.checkStudentId = function()
  {
  	 var result = [];
  	 var data = "studentId="+$scope.studentId;
	   var xhttp = new XMLHttpRequest();
	   xhttp.onreadystatechange = function() {
	   if (this.readyState == 4 && this.status == 200) {
	   		result = JSON.parse(this.responseText);
	   		$scope.fname = result.fname;
	   		$scope.lname = result.lname;
	   		$scope.entryYear = result.year;
        // $scope.gender = "m";
        // console.log($scope.gender);
	   }
	};
	xhttp.open("POST", baseUrl+"register/checkStudentId",false);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
  }
  $scope.checkCompany = function()
  {	
  	 var result = [];
  	 var data = "name="+$scope.companyName;
	   var xhttp = new XMLHttpRequest();
	   xhttp.onreadystatechange = function() {
	   if (this.readyState == 4 && this.status == 200) {
	   		result = JSON.parse(this.responseText);
	   		$scope.companyName = result.name;
	   		$scope.companyAddress = result.address;
	   		$scope.companyCity = result.city;
	   		$scope.companyPhone = result.phone;
	   		$scope.companyWebsite = result.website;

        var companyWork = result.work;
        for (var i = 0; i < companyWork.length; i++) {
          $(document).ready(
            function(){
                var theValue = companyWork[i];
                $('#companyWork option[value=' + theValue + ']')
                    .attr('selected',true);
            });
        }

        $("#companyWork").selectpicker("refresh");
        	   }
        	};

	xhttp.open("POST", baseUrl+"register/checkCompany",false);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
  }
});


//Auto Complete
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

  var listCompany = [];
  var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
        listCompany = JSON.parse(this.responseText);
     }
  };
  xhttp.open("GET", baseUrl+"register/listCompany",false);
  xhttp.send();


$('.listCompany').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'listCompany',
  source: substringMatcher(listCompany)
});

