# CPE-ALUMNI (public file) README #

This repository contains public file from Project **ALUMNI CPE**

**app, public, index** repository is required

### File Directory ###
- **assets** - _web template file (contains stylesheet, script)_
- **css** - _additional css stylesheet_
- **image** - _image file use in website_
- **js** - _additonal javascript file (angular, chart file, additional function)_
- **upload** - _uploaded image by user_


*source file contains in another repository*

### Installation ###
- - delete **.git** file ( or type `rm -rf .git` in command line )
- create folder to contains web **assets** and **source** files
- download **app** repository into project folder
- download **public** repository into the project folder
- download **index** repository into the project folder then move files outside from index folder

### File Structure ###
    cpealumni
        |_ app
        |_ public
        |_ .htaccess
        |_ .htrouter.php
        |_ composer.bat
        |_ index.html

